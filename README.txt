INSTALLATION

Install this theme like you would any other, except that you must also have the
Zen theme in your repository.

LICENSE
This theme is licensed under the GPL license. You are free to use it, share it,
modify it, etc, as long as this license stays in place.
